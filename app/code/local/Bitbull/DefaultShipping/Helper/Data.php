<?php
/**
 * @category Bitbull
 * @package  Bitbull_DefaultShipping
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
*/ 
class Bitbull_DefaultShipping_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_COUNTRY_DEFAULT = 'general/country/default';

    public function getCountryId()
    {
        $chosenCountryId = Mage::getSingleton('checkout/session')->getCountryId();

        if ($chosenCountryId) {
            return $chosenCountryId;
        } else {
            return Mage::getStoreConfig(self::XML_PATH_COUNTRY_DEFAULT);
        }
    }

    public function setCountryId($countryId)
    {
        Mage::getSingleton('checkout/session')->setCountryId($countryId);
    }
}